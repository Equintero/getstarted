from django.db import models
from django.contrib.auth.models import User

class Type(models.Model):
        title = models.CharField(max_length=255, blank=True, null=True)
        description = models.TextField(blank=True, null=True)
        def __unicode__(self):
                return self.title

class Step(models.Model):
        title = models.CharField(max_length=255, blank=True, null=True)
        description = models.TextField(blank=True, null=True)
        def __unicode__(self):
                return self.title

class StepOrder(models.Model):
    studentType = models.ForeignKey(Type, related_name='stepOrder')
    stepNumber = models.IntegerField(blank=True, null=True)
    step = models.ForeignKey(Step, related_name='step')
    def __unicode__(self):
                return "Step"+str(self.stepNumber)+" for "+self.studentType.title

