from django.contrib import admin
from getstarted.models import *

class StepOrderInline(admin.StackedInline):
    model = StepOrder
    extra = 0
    exclude = ['stepNumber',]

class TypeAdmin(admin.ModelAdmin):
    inlines = [StepOrderInline,]

admin.site.register(Type, TypeAdmin)
admin.site.register(Step)
admin.site.register(StepOrder)
