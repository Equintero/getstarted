from django.conf.urls import patterns, include, url
from getstarted.views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', Index),
    url(r'^admin/', include(admin.site.urls)),
    # -- API routs
    url(r'^type/(?P<pk>\d+)/$', TypeDetail.as_view(), name='type-detail'),
    url(r'^type/$', TypeList.as_view(), name='type-list'),
    url(r'^step/(?P<pk>\d+)/$', StepDetail.as_view(), name='step-detail'),
    url(r'^step/$', StepList.as_view(), name='step-list'),
)
